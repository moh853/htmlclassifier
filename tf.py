import os
import sys
import json
import glob
import numpy as np
import math
from parse import parse

labels = os.getenv('labels').split(',')

path = sys.argv[1]

docs = json.load(open(path + '/output/docs.json'))
word_list = json.load(open(path + '/output/word_list.txt'))

number_of_docs = len(docs)
number_of_words = len(word_list)

print "Step 3"
# Step 3 TF calculation
print 'Making a TF matrix of size {}x{}'.format(number_of_docs, number_of_words)
tf = np.zeros((number_of_docs, number_of_words))
for d_i, d in enumerate(docs):
    for w in d:
        w_i = word_list.index(w)
        tf[d_i][w_i] += 1

print 'Saving to file now...'
np.savetxt(path + '/output/tf', tf, delimiter=',')
