import os
import sys
import random
import numpy as np
from pyspark import SparkContext
from pyspark.mllib.classification import NaiveBayes, NaiveBayesModel
from pyspark.mllib.regression import LabeledPoint

n_tests = int(os.getenv('n_tests'))
master = os.getenv("MASTER")
path = sys.argv[1]
sc = SparkContext(master, 'pyspark')

data = sc.textFile('/scratch/mahmadza/data/output/*.csv')
p = data.map(lambda x: [x.split(',')[0], x.split(',')[1:]]).map(lambda x: LabeledPoint(float(x[0]), [float(i) for i in x[1]]))

accuracies = []
for i in range(n_tests):
    rand = random.randint(0, 32000)
    training, test = p.randomSplit([0.6, 0.4], rand)
    model = NaiveBayes.train(training, 1.0)
    predictionAndLabel = test.map(lambda p : (model.predict(p.features), p.label))
    accuracy = 1.0 * predictionAndLabel.filter(lambda (x, v): x == v).count() / test.count()
    accuracies.append(accuracy)
    print 'Accuracy: ' + str(accuracy)

print 'Average accuracy: ' + str(np.mean(accuracies))
print 'All accuracy values: ' + str(accuracies)
model.save(sc, path + '/output/model')

