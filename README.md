# HTML-Classifier #

This is an html classifier using Spark's Naive Bayes library for CS526. There are a couple of python packages it depends on:

- Flute (https://github.com/auxiliary/flute)
- NLTK
- python-goose
- numpy
- BeautifulSoup 4 (This is included in the repo)