import os
import sys
import json
import glob
import numpy as np
import math
from parse import parse

labels = os.getenv('labels').split(',')

path = sys.argv[1]

docs = json.load(open(path + '/output/docs.json'))
word_list = json.load(open(path + '/output/word_list.txt'))
data = json.load(open(path + '/output/structured.json'))
tf = np.loadtxt(path + '/output/tf', delimiter=',')
n_docs_with_word = json.load(open(path + '/output/n_docs_with_word'))

number_of_docs = len(docs)
number_of_words = len(word_list)

print "Step 4"
# Step 4 Calculate TF-IDF and save matrix to file for each label
for label_i, label in enumerate(labels):
    print "Label {}".format(label)
    tfidf = []
    for d_i, d in enumerate(docs):
        if label not in data or d not in data[label]:
            continue
        row = np.zeros(number_of_words + 1)
        row[0] = label_i
        for w_i, w in enumerate(word_list):
            row[w_i + 1] = tf[d_i][w_i] * math.log(number_of_docs / (float(n_docs_with_word[str(w_i)])))
        tfidf.append(row)

    np.savetxt(path + '/output/' + label + '.csv', tfidf, delimiter=',')
                
