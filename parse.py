import os
import sys
import re
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from bs4 import BeautifulSoup as Soup

def tokenize(content):
    rx = re.compile('[\W_]')
    stemmer = SnowballStemmer('english')
    stops = set(stopwords.words('english'))
    content = rx.sub(' ', content).split()
    cleaned = []
    for word in content:
        word = stemmer.stem(word)
        if word not in stops:
            cleaned.append(word)

    return cleaned

def parse(txt):
    allowed_topics = os.getenv('labels').split(',')
    #rx = re.compile('[-.,&#;\(\)/\"\'\n]')
    rx = re.compile('[\W_]')
    unicode_rx = re.compile(r'[^\x00-\x7F]+')
    stemmer = SnowballStemmer('english')
    stops = set(stopwords.words('english'))

    results = []
    soup = Soup(txt, 'html.parser')
    for x in soup.findAll('topics'):
        if x != '' and x.text != '':
            topics = []
            n_topics_added = 0
            for topic in x.contents:
                topic = topic.contents[0]
                if topic is None:
                    continue
                else:
                    if topic in allowed_topics:
                        topics.append(topic)
                        n_topics_added += 1
            
            if n_topics_added == 0:
                continue

            body = x.parent.find('body')
            if (body is not None):
                body = unicode_rx.sub(' ', body.text)
                body = str(body)
                body = tokenize(body)
            else:
                body = ''

            if len(body) != 0:
                results.append([topics, body])

    return results



