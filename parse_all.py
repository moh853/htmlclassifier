import os
import sys
import glob
import numpy as np
import math
import json
from parse import parse

labels = os.getenv('labels').split(',')
path = sys.argv[1]
files = glob.glob(path + '/*.sgm')
results = []
for f in files:
    text = open(f).read()

    print 'parsing {}'.format(f)
    parsed = parse(text)
    for res in parsed:
        results.append(res)

json.dump(results, open(path + '/output/parsed.json', 'w'))

