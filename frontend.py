import os
import sys
import goose
from classify import classify
from pyspark import SparkContext

url = sys.argv[2]
master = os.getenv("MASTER")
sc = SparkContext(master, 'pyspark')
gconf = goose.Configuration()
gconf.enable_image_fetching = False
goo = goose.Goose(gconf)
content = goo.extract(url).cleaned_text
classify(sc, content)
