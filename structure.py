import os
import sys
import json
import glob
import numpy as np
import math
from parse import parse

labels = os.getenv('labels').split(',')

path = sys.argv[1]

results = json.load(open(path + '/output/parsed.json'))

# Getting correct form of data and step 1
docs = []
data = {} # of the form label: documents
for x in results:
    docs.append(x[1])
    for label in x[0]:
        if label not in data:
            data[label] = []
        data[label].append(x[1])


json.dump(data, open(path + '/output/structured.json', 'w'))
json.dump(docs, open(path + '/output/docs.json', 'w'))


