import os
import sys
import json
import glob
import numpy as np
import math
from parse import parse

labels = os.getenv('labels').split(',')
path = sys.argv[1]

data = json.load(open(path + '/output/structured.json'))
docs = json.load(open(path + '/output/docs.json'))
print "Step 2"
# Step 2
word_list = []
n_docs_with_word = {}
for doc in docs:
    doc_set = set(doc)
    for word in doc_set:
        if word in word_list:
            word_index = word_list.index(word)
            n_docs_with_word[word_index] += 1
        else:
            word_list.append(word)
            word_index = word_list.index(word)
            n_docs_with_word[word_index] = 1

print '{} words'.format(len(word_list))
json.dump(word_list, open(path + '/output/word_list.txt', 'w'))
json.dump(n_docs_with_word, open(path + '/output/n_docs_with_word', 'w'))
