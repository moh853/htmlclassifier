import os
import sys
import json
import numpy as np
import math
from parse import tokenize
from pyspark.mllib.classification import NaiveBayes, NaiveBayesModel
from pyspark import SparkContext

def classify(sc, content):
    labels = os.getenv('labels').split(',')
    path = sys.argv[1]

    content = tokenize(content)
    word_list = json.load(open(path + '/output/word_list.txt'))
    docs = json.load(open(path + '/output/docs.json'))
    n_docs_with_word = json.load(open(path + '/output/n_docs_with_word'))

    number_of_docs = len(docs)
    number_of_words = len(word_list)

    # Calculate tf for this new content
    tf = np.zeros(len(word_list))
    for word in content:
        if word in word_list:
            w_i = word_list.index(word)
            tf[w_i] += 1


    vector = np.zeros(number_of_words)
    for w_i, w in enumerate(word_list):
        vector[w_i] = tf[w_i] * math.log(number_of_docs / (float(n_docs_with_word[str(w_i)])))

    model = NaiveBayesModel.load(sc, path  + '/output/model')
    prediction = int(model.predict(vector))
    print 'Predicted to be about ' + labels[prediction]

